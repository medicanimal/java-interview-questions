package com.medicanimal.ex3;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();

        Animal animal = new Animal("Bear");

        Dog dog = (Dog) new Animal("Bobby");
        dog.setColor("black");

        Animal fish = new Fish("Nemo");
        ((Fish) fish).setColor("red");

        animals.add(animal);
        animals.add(dog);
        animals.add(fish);

        for (Animal currentAnimal : animals) {
            System.out.println("Name: " + currentAnimal.getName());
        }

    }

    
}



















class Dog extends Animal {
    private String gender;
    private String color;

    public Dog(String name) {
        super(name);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

class Fish extends Animal {

    private String color;


    public Fish(String name) {
        super(name);
    }

    public void setColor(String color) {
        this.color = color;
    }
}

class Animal {
    private String name;

    public Animal(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}