package com.medicanimal.ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Category dogCtg1 = new Category("Dog");
        Category dogCtg2 = new Category("Food");
        Category catCtg1 = new Category("Cat");
        Category catCtg2 = new Category("Food");
        Product dogProduct = new Product();
        dogProduct.addCategory(dogCtg1);
        dogProduct.addCategory(dogCtg2);

        Product catProduct = new Product();
        catProduct.addCategory(catCtg1);
        catProduct.addCategory(catCtg2);

        printUniqueCategories(dogProduct,catProduct);

    }

    public static void printUniqueCategories(Product... products){

    }
    
}




























class Product {
    private List<Category> categories = new ArrayList<>();

    public Product(){
    }


    public List<Category> getCategories() {
        return categories;
    }

    public void addCategory(Category category){
        categories.add(category);
    }
}

class Category{
    private String name;

    public Category(String name){
        this.name = name;
    }
    public Category(){
    }

    public String getName() {
        return name;
    }
}