package com.medicanimal.ex4;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto on 2/12/16.
 */
public class ex4 {

    public static void main(String[] args) {
        Product product = new Product();

        Category dogCtg1 = new Category("Dog");
        Category dogCtg2 = new Category();
        Category dogCtg3 = new Category("");

        product.addCategory(dogCtg1);
        product.addCategory(dogCtg2);
        product.addCategory(dogCtg3);

        List<Category> categories = product.getCategories();

        for (Category category : categories) {
            System.out.println("Category name length: " + category.getName().length());
        }
    }
}


























class Product {
    private List<Category> categories = new ArrayList<>();

    public Product(){
    }

    public List<Category> getCategories() {
        return categories;
    }
    public void addCategory(Category category){
        categories.add(category);
    }
}

class Category{
    private String name;

    public Category(String name){
        this.name = name;
    }
    public Category(){

    }

    public String getName() {
        return name;
    }

}
