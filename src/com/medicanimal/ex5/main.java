package com.medicanimal.ex5;

/**
 * Created by roberto on 2/23/16.
 */
public class main {

    public static String funcX (String s) {
        if (s == null || s.length() == 1 || s.length() == 0) {
            return s;
        } else {
            return funcX(s.substring(1)) + s.substring(0, 1);
        }
    }

    public static void main(String[] args) {
        String name="dogs";
        System.out.println(funcX(name));
    }
}
