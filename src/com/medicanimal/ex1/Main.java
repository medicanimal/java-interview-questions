package com.medicanimal.ex1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        ArrayList<String> prd1Categories = new ArrayList<String>(Arrays.asList("Dog", "Accessories", "Food", "Treat"));
        PlainProduct dog = new PlainProduct(prd1Categories);

        ArrayList<String> prd2Categories = new ArrayList<String>(Arrays.asList("Cat", "Accessories", "Supplements", "Food"));
        PlainProduct cat = new PlainProduct(prd2Categories);

        // Print all the unique categories from the objects 'cat' and 'dog'














    }
}

class PlainProduct {
    private List<String> categories;

    public PlainProduct(List<String> categories){
        this.categories=categories;
    }

    public List<String> getCategories() {
        return categories;
    }
}